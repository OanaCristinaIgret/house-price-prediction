# -*- coding: utf-8 -*-
"""Untitled0.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/13CStDSaD41WlGZcLoUIlnZhqDkIPkwrj
"""

from google.colab import files
uploaded=files.upload()

for fn in uploaded.keys():
  print('User uploaded file "{name}" with length {length} bytes'.format(
      name=fn, length=len(uploaded[fn])))

import io
import pandas as pd

df_train=pd.read_csv(io.StringIO(uploaded['train.csv'].decode('utf-8')))
print(df_train.head())

df_test=pd.read_csv(io.StringIO(uploaded['test.csv'].decode('utf-8')))
print(df_test.head())

missing_count=(df_train.isnull().sum()/len(df_train))*100
missing_count=missing_count[missing_count>0]
print(missing_count.sort_values())

import seaborn as sns
sns.distplot(df_train['SalePrice'])

import numpy as np

target=np.log(df_train['SalePrice'])
sns.distplot(target)

numeric_data=df_train.select_dtypes(include=[np.number])
categorical_data=df_train.select_dtypes(exclude=[np.number])
print('There are {0} numerical and {1} categorical features in the training data'.
      format(numeric_data.shape[1], categorical_data.shape[1]))

del numeric_data['Id']
numeric_data.columns

corr=numeric_data.corr()
sns.heatmap(corr)

# Commented out IPython magic to ensure Python compatibility.
import matplotlib.pyplot as plt
# %matplotlib inline
df_train.groupby(['YrSold', 'MoSold']).Id.count().plot(kind='bar',figsize=(14,4))
plt.title("When was the property sold?")
plt.show()

df_train.groupby(['Neighborhood']).Id.count().sort_values().plot(kind='bar',figsize=(10,4))
plt.title("Where are the most of the properties located?")
plt.show()

f=pd.melt(df_train, value_vars=sorted(numeric_data))
g=sns.FacetGrid(f, col='variable', col_wrap=4, sharex=False, sharey=False)
g=g.map(sns.distplot, 'value')

df_train['MSSSubClass']=df_train.MSSubClass.apply(lambda x: str(x))
df_train['MoSold']=df_train.MoSold.apply(lambda x: str(x))
df_train['YrSold']=df_train.YrSold.apply(lambda x: str(x))

numeric_data=df_train.select_dtypes(include=[np.number])
categorical_data=df_train.select_dtypes(exclude=[np.number])
print('There are {0} numerical and {1} categorical features in the training data'.
      format(numeric_data.shape[1], categorical_data.shape[1]))

f=pd.melt(df_train, value_vars=sorted(categorical_data))
g=sns.FacetGrid(f, col='variable', col_wrap=4,sharex=False, sharey=False)
plt.xticks(rotation='vertical')
g=g.map(sns.countplot, 'value')
[plt.setp(ax.get_xticklabels(), rotation=50) for ax in g.axes.flat]
g.fig.tight_layout()
plt.show()

df_train.Alley.replace.({'Grvl':1,'Pave':2},inplace=True)
df_train['Alley'].unique()
array([nan,1.,2.])





df_train.LotShape.replace({'Reg':1,'IR1':2,'IR2':3,'IR3':4}, inplace=True)

df_train.BldgType.replace({'1Fam':1, '2fmCon':2,'Duplex':3,'TwnhsE':4,'Twnhs':5}, inplace=True)

df_train.LandContour.replace({'Low':1,'HLS':2, 'Bnk':3,'AllPub':4}, inplace=True)

df_train.Utilities.replace({'ELO':1,'NoSewa':2, 'NoSewr':3, 'AllPub':4}, inplace=True)

df_train.LandSlope.replace({'Sev':1,'Mod':2,'Gtl':3}, inplace=True)

df_train.ExterQual.replace({'Po':1, 'Fa':2,'Ta':3, 'Gd':4, 'Ex':5}, inplace=True)

df_train.BsmtQual.replace({'Po':1, 'Fa':2,'Ta':3, 'Gd':4, 'Ex':5}, inplace=True)

df_train.BsmtFinType1.replace({'Unf':1, 'LvQ':2, 'Rec':3,'BLQ':4, 'ALQ':5,'GLQ':6}, inplace=True)

df_train.BsmtFinType2.replace({'Unf':1, 'LvQ':2, 'Rec':3,'BLQ':4, 'ALQ':5,'GLQ':6}, inplace=True)

df_train.HeatingQC.replace({'Po':1, 'Fa':2,'Ta':3, 'Gd':4, 'Ex':5}, inplace=True)

df_train.KitchenQual.replace({'Po':1, 'Fa':2,'Ta':3, 'Gd':4, 'Ex':5}, inplace=True)

df_train.FireplaceQu.replace({'Po':1, 'Fa':2,'Ta':3, 'Gd':4, 'Ex':5}, inplace=True)

df_train.GarageFinish.replace({'Unf':1,'RFn':2,'Fin':3}, replace=True)

df_train.GarageQual.replace({'Po':1, 'Fa':2,'Ta':3, 'Gd':4, 'Ex':5}, inplace=True)

df_train.PavedDrive.replace({'N':1,'P':2,'Y':3}, replace=True)

df_train.PoolQC.replace({'Fa':2,'Ta':3, 'Gd':4, 'Ex':5}, inplace=True)

cat_to_num_features =['Alley', 'LotShape', 'LandContour', 'Utilites', 'LandSlope', 'ExterQual', 'ExterCond', 'BsmtQual', 'BsmtCond', 'BsmtExposer', 'BsmtFinType1', 'BsmtFinType2', 'HeatingQC', 'KitchenQual', 'Functional', 'FireplaceQu', 'GarageFinish', 'GarageQual', 'GarageCond', 'PaveDrive', 'PoolQC']
df_train[cat_to_num_features]=df_train[cat_to_num_features].fillna(0)

numeric_data=df_train.select_dtypes(include=[np.number])
categorical_data=df_train.select_dtypes(exclude=[np.number])
print('there are {0} numerical and {1} categorical features in the training data'.format(numeric_data.shape[1], categorical_data.shape[1]))

f=pd.melt(df_train,id_vars=['SalePrice'] ,value_vars=sorted(categorical_data))
g=sns.FacetGrid(f, col='variable', col_wrap=3,sharex=False, sharey=False, size=4)
plt.xticks(rotation='vertical')
g=g.map(sns.boxplot, 'value','SalePrice')
[plt.setp(ax.get_xticklabels(), rotation=50) for ax in g.axes.flat]
g.fig.tight_layout()
plt.show()

import scipy.stats
cat_features=categorical_data.columns
df_train[cat_features]=df_train[cat_features].fillna('Missing')
anova={'feature':[], 'f':[],'p':[]}
for cat in cat_features:
  group_prices=[]
  for group in df_train[cat].unique():
    group_prices.append(df_train[df_train[cat]==group]['SalesPrice'].values)
    f,p=scipy.status.f_oneway(*group_prices)
    anova['feature'].append(cat)
    anova['f'].append(f)
    anova['p'].append(p)
anova=pd.DataFrame(anova)
anova=anova[['feature','f','p']]
anova.sort_values('p',inplace=True)

p<0.05

plt.figure(figsize=(8,5))
sns.barplot(anova.feature, np.log(1./anova['p']))
plt.xticks(rotation=90)
plt.show()

missing_values=(df_train[cat_features]=='Missing'.sum().sort_values(ascending=False)
missing_values[missing_values>0]

df_train.loc[df_train.Electrical=='Missing', 'Electrical']=df_train.Electrical.mode()[0]



#Linear Plot
plt.figure(figsize=(10,5))
sns.regplot(df_train.GrLivArea, df_train.SalePrice, scatter_kws={'alpha':0,3})
plt.show()